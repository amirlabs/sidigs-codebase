package base.sidigs.base.data

sealed class ResultState<T> {
    class Loading<T> : ResultState<T>()
    class Default<T> : ResultState<T>()
    class Empty<T> : ResultState<T>()
    data class Success<T>(val data: T) : ResultState<T>()
    data class Failure<T>(val throwable: Throwable?, val message: String?) : ResultState<T>()

    companion object {

        fun <T> loading(): ResultState<T> = Loading()
        fun <T> default(): ResultState<T> = Default()
        fun <T> success(data: T): ResultState<T> = Success(data)
        fun <T> empty(): ResultState<T> = Empty()
        fun <T> fail(throwable: Throwable, message: String?): ResultState<T> = Failure(throwable, message)
        fun <T> fail(message: String?): ResultState<T> = Failure(null, message)
    }

    fun <S> map(mapper: (T) -> S):ResultState<S>{
        return when(this){
            is Success -> success(mapper(this.data))
            else -> this as ResultState<S>
        }
    }
}