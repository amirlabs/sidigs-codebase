package base.sidigs.base.utils.core

import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.media.RingtoneManager
import androidx.annotation.DrawableRes
import androidx.core.app.NotificationCompat

/**
 * @Ahmad Amirudin.
 * @since 11-Mar-21.
 */

/**
 * Method to displaying notification
 * @param ctx context from notification displayed
 * @param channelId the notification channel id, required from Android Oreo
 * @param title the title of the notification
 * @param message the content text of the notification
 * @param notifIcon drawable resource id for notification
 * @param pendingIntent intent or operation to do when notification clicked
 */
fun displayNotification(
    ctx: Context,
    channelId: String,
    title: String,
    message: String,
    @DrawableRes notifIcon: Int,
    pendingIntent: PendingIntent? = null
) {
    val notificationManager = ctx.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

    val notificationBuilder = NotificationCompat.Builder(ctx, channelId).apply {
        setSmallIcon(notifIcon)
        setContentTitle(title)
        setContentText(message)
        setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
        setContentIntent(pendingIntent)
        setStyle(NotificationCompat.BigTextStyle().bigText(message))
    }

    notificationManager.notify(123, notificationBuilder.build())
}