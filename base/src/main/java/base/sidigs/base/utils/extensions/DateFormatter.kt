package base.sidigs.base.utils.extensions

import java.text.SimpleDateFormat
import java.util.*

/**
 * Extension method from string to convert date from api response
 * @param newPattern new date pattern from string
 * @param isTimeZone
 * @param dayname the locale of the string, default value = Locale.getDefault()
 * @return String if date convert successfuly or Exception
 */
fun String?.formatDate(
    newPattern: String,
    isTimeZone: Boolean = true,
    dayname: Boolean = false
): String? {
    val inputPattern = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())
    val inputPatternTimezone = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.getDefault())
    val outputPattern = if (newPattern.isEmpty()) {
        "dd MMMM yyyy HH:mm:ss"
    } else {
        newPattern
    }

    val inputFormat = if (isTimeZone) inputPatternTimezone
    else inputPattern
    var calendarday = 0
    val outputFormat = SimpleDateFormat(outputPattern, Locale("id", "ID"))
    var date: Date? = null
    var str: String? = null
    try {
        date = inputFormat.parse(this)

        val calendar = Calendar.getInstance()
        calendar.time = date
        calendarday = calendar.get(Calendar.DAY_OF_WEEK)

        str = outputFormat.format(calendar.time)
    } catch (e: Exception) {
        e.printStackTrace()
    }
    return if (dayname) getDayName(calendarday) + ", " + str
    else str
}

/**
 * Extension method from string to convert date to new pattern
 * @param inputPattern previous pattern from string
 * @param newFormat new pattern from string
 * @param dayname the locale of the string, default value = Locale.getDefault()
 * @return String if date convert successfuly or Exception
 */
fun String?.convertDate(
    inputPattern: String?,
    newFormat: String,
    dayname: Boolean = false
): String? {
    val inputFormat = SimpleDateFormat(inputPattern, Locale.getDefault())

    var calendarday = 0
    val outputFormat = SimpleDateFormat(newFormat, Locale("id", "ID"))
    var date: Date? = null
    var str: String? = null
    try {
        date = inputFormat.parse(this)

        val calendar = Calendar.getInstance()
        calendar.time = date
        calendarday = calendar.get(Calendar.DAY_OF_WEEK)

        str = outputFormat.format(calendar.time)
    } catch (e: Exception) {
        e.printStackTrace()
    }
    return if (dayname) getDayName(calendarday) + ", " + str
    else str
}

/**
 * Extension method from string to parse string to date
 * @param toFormat format date from string
 * @param locale the locale of the string, default value = Locale.getDefault()
 * @return Date if parsing success and null if there's exception
 */
fun String.toDate(toFormat: String, locale: Locale = Locale.getDefault()): Date? {
    return try {
        SimpleDateFormat(toFormat, locale).parse(this)
    } catch (t: Throwable) {
        null
    }
}

/**
 * Extension method from date to format date to string
 * @param toFormat format string to be formatted
 * @param locale the locale of the result string, default value = Locale.getDefault()
 * @return String if formatting success and null if there's exception
 */
fun Date.toString(toFormat: String, locale: Locale = Locale.getDefault()): String? {
    return try {
        SimpleDateFormat(toFormat, locale).format(this)
    } catch (t: Throwable) {
        null
    }
}






fun getDayName(day: Int): String {
    return when (day) {
        Calendar.SUNDAY -> "Minggu"
        Calendar.MONDAY -> "Senin"
        Calendar.TUESDAY -> "Selasa"
        Calendar.WEDNESDAY -> "Rabu"
        Calendar.THURSDAY -> "Kamis"
        Calendar.FRIDAY -> "Jumat"
        Calendar.SATURDAY -> "Sabtu"
        else -> "Wrong Day"
    }
}