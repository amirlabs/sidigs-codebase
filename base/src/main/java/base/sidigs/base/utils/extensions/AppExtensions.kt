package base.sidigs.base.utils.extensions

import android.content.Context
import android.graphics.drawable.Drawable
import android.view.View
import android.widget.Toast
import base.sidigs.base.utils.core.Toasty
import com.afollestad.materialdialogs.MaterialDialog
import com.google.android.material.snackbar.Snackbar

fun Context?.displayToast(type:Int,msg:String?,duration: Int=Toast.LENGTH_SHORT){
    when(type){
        Toasty.SUCCESS -> this?.let { Toasty.success(it,msg,duration).show() }
        Toasty.ERROR -> this?.let { Toasty.error(it,msg,duration).show() }
        Toasty.INFO -> this?.let { Toasty.info(it,msg,duration).show() }
        Toasty.WARNING -> this?.let { Toasty.warning(it,msg,duration).show() }
        Toasty.NORMAL -> this?.let { Toasty.normal(it,msg,duration).show() }
    }
}


fun Context?.toastNormal(msg:String?,duration: Int=Toast.LENGTH_SHORT, icon:Drawable?=null){
    this?.let { Toasty.normal(it,msg,duration,icon).show() }
}

fun Context?.toastSuccess(msg:String?,duration: Int=Toast.LENGTH_SHORT, withIcon:Boolean=false){
    this?.let { Toasty.success(it,msg,duration,withIcon).show() }
}

fun Context?.toastWarning(msg:String?,duration: Int=Toast.LENGTH_SHORT, withIcon:Boolean=false){
    this?.let { Toasty.warning(it,msg,duration,withIcon).show() }
}

fun Context?.toastError(msg:String?,duration: Int=Toast.LENGTH_SHORT, withIcon:Boolean=false){
    this?.let { Toasty.error(it,msg,duration,withIcon).show() }
}

fun Context?.toastInfo(msg:String?,duration: Int=Toast.LENGTH_SHORT, withIcon:Boolean=false){
    this?.let { Toasty.info(it,msg,duration,withIcon).show() }
}


/**
 * Method to display SnackBar
 * @param message message to display
 * @param isItLong is the SnackBar gonna be short or long?
 * @param action pair of string and action that can be attached to snackBar, nullable
 */
fun displaySnackBar(
    view: View,
    message: String,
    isItLong: Boolean = false,
    action: Pair<String, View.OnClickListener>? = null
) {
    val snack = Snackbar.make(view, message, if (isItLong) Snackbar.LENGTH_LONG else Snackbar.LENGTH_SHORT)
    action?.let {
        snack.setAction(it.first, it.second)
    }

    snack.show()
}

/**
 * Method to display dialog
 * @param
 * @param title title of dialog
 * @param message message of dialog
 * @param positiveAction pair of string and action for positive button, the action is nullable
 * @param negativeAction pair of string and action for negative button, nullable
 */
fun displayDialog(
    ctx: Context?,
    title: String,
    message: String,
    positiveAction: Pair<String, (() -> Unit)?>,
    negativeAction: Pair<String, (() -> Unit)?>? = null,
    autoDismiss: Boolean = false
) {
    if (ctx != null) {
        MaterialDialog(ctx).show {
            title(text = title)
            cornerRadius(literalDp = 12f)
            cancelable(autoDismiss)
            message(text = message)
            positiveButton(text = positiveAction.first) {
                positiveAction.second?.invoke()
            }
            negativeAction?.let { pair ->
                this.negativeButton(text = pair.first) {
                    pair.second?.invoke()
                }
            }
        }
    }
}
