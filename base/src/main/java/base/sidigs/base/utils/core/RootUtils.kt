package base.sidigs.base.utils.core

import android.content.Context
import base.sidigs.base.utils.security.EmulatorDetector
import com.scottyab.rootbeer.RootBeer

enum class SecurityCheck {

    EMULATOR, ROOTED, SAFE
}

/**
 * Method to check device integrity
 * @return is the device an emulator, rooted, or safe
 */
fun Context.securityCheck(): SecurityCheck {
    return when {
        isEmulator(this) -> SecurityCheck.EMULATOR
        isRooted(this) -> SecurityCheck.ROOTED
        else -> SecurityCheck.SAFE
    }
}

/**
 * @param ctx context
 * @return is device an emulator
 */
private fun isEmulator(ctx: Context): Boolean {
    return try {
        EmulatorDetector.isEmulator(ctx)
    } catch (t: Throwable) {
        false
    }
}

/**
 * @param ctx context
 * @return is device rooted
 */
private fun isRooted(ctx: Context): Boolean {
    val rootBeer = RootBeer(ctx)
    return if (rootBeer.isRooted)
        !rootBeer.isRootedWithBusyBoxCheck
    else
        false
}