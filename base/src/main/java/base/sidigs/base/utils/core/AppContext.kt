package base.sidigs.base.utils.core

import android.annotation.SuppressLint
import android.content.Context
import base.sidigs.base.utils.core.AppContext.mContext

/**
 * Class for define Application Context
 *
 * @author Ahmad Amirudin.
 * @since 11-Mar-21.
 * @property mContext the app's context
 */
@SuppressLint("StaticFieldLeak")
object AppContext {

    private var mContext: Context? = null

    /**
     * Init context
     * @param context Application Context
     */
    private fun init(context: Context) {
        mContext = context
    }

    /**
     * Check if context is null or not
     * @return Application Context
     */
    private fun getContext() = mContext ?: throw IllegalStateException("call init first")

    @JvmStatic
    fun setBaseContext(context: Context) {
        init(context)
    }

    @JvmStatic
    fun get(): Context {
        return getContext()
    }
}