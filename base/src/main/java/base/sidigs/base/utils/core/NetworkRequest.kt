package base.sidigs.base.utils.core

import base.sidigs.base.data.ResultState
import base.sidigs.base.data.web.model.DevApiResponse
import com.bumptech.glide.load.HttpException
import java.lang.Exception
import java.net.ConnectException
import java.net.SocketTimeoutException
import java.net.UnknownHostException

fun <T : DevApiResponse<*>, S> T.catchAndGetData(): ResultState<S> {
    return try {
        if (this.data!=null){
           ResultState.success(this.data as S)
        }else
            ResultState.fail(this.message)
//        when (this.status) {
//            200 -> ResultState.success(this.data as S)
//            else -> ResultState.fail(this.message)
//        }
    } catch (ex: Exception) {
        ex.printStackTrace()
        when {
            ex.message!!.contains("404") -> ResultState.fail("Halaman tidak ditemukan")
            ex.message!!.contains("400") -> ResultState.fail("Sistem sedang offline")
            ex.message!!.contains("401") -> ResultState.fail("Sesi telah berakhir. Silahkan login aplikasi lagi")
            else -> {
                return when (ex) {
                    is SocketTimeoutException -> ResultState.fail("Gagal Terhubung, Coba Lagi")
                    is UnknownHostException -> ResultState.fail("Cek Koneksi Internet Anda")
                    is HttpException -> ResultState.fail("Cek Koneksi Internet Anda")
                    is ConnectException -> ResultState.fail("Cek Koneksi Internet Anda")
                    else -> ResultState.fail("Maaf, Sesuatu Bermasalah")
                }
            }
        }
    }
}