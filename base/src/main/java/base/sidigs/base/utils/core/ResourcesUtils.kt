package base.sidigs.base.utils.core

import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import androidx.core.content.res.ResourcesCompat

/**
 * Method to get drawable from resource
 * @param id Drawable Id
 * @return desired drawable
 */
fun getDrawable(@DrawableRes id: Int) = ResourcesCompat.getDrawable(AppContext.get().resources, id, null)

/**
 * Method to get Color from resource
 * @param id Color Id
 * @return desired color
 */
fun getColor(@ColorRes id: Int) = ResourcesCompat.getColor(AppContext.get().resources, id, null)

/**
 * Method to get String from resource
 * @param id String Id
 * @return desired string
 */
fun getString(@StringRes id: Int) = AppContext.get().getString(id)

/**
 * Method to get array string from resource
 * @param id String Id
 * @return desired string array
 */
fun getStringArray(id: Int) = AppContext.get().resources.getStringArray(id)