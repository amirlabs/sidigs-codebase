package base.sidigs.base

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.media.RingtoneManager
import android.os.Build
import androidx.multidex.MultiDexApplication
import base.sidigs.base.di.preferenceModule
import base.sidigs.base.di.utilityModule
import base.sidigs.base.utils.core.AppContext
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level
import org.koin.core.module.Module
import timber.log.Timber


/**
 * Application class for maintaining global state
 *
 * @author Ahmad Amirudin
 * @since 11-Mar-21.
 * @sample base.sidigs.devcode.SampleApplication
 */
abstract class DevApplication : MultiDexApplication(){

    /**
     * Define series of action when application initialized
     */
    abstract fun initApplication()

    /**
     * Method to declare list of Koin Modules within apps
     * @return list of koin modules
     */
    abstract fun defineKoinModules(): ArrayList<Module>

    /**
     * Method to define notification channels
     * @return list of notification channels
     */
    abstract fun defineNotificationChannel(): List<NotificationChannel>

    /**
     * Method to declare action on global state
     * For now, it's used to :
     * 1. start Koin Injection
     * 2. Timber Plant Log
     * 3. Setting App Context
     */
    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(applicationContext)
            modules(listOf(preferenceModule, utilityModule))
            modules(defineKoinModules())
            androidLogger(Level.INFO)
        }
        initApplication()

        createNotificationChannel()
        Timber.plant(Timber.DebugTree())
        AppContext.setBaseContext(applicationContext)
    }

    /**
     * Method to create notification from channels
     */
    private fun createNotificationChannel() {
        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        defineNotificationChannel().forEach {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                notificationManager.createNotificationChannel(
                    it.apply {
                        enableLights(true)
                        setShowBadge(true)
                        setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION), null)
                        lockscreenVisibility = Notification.VISIBILITY_PUBLIC
                    }
                )
            }
        }
    }
}