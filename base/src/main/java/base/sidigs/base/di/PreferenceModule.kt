package base.sidigs.base.di

import base.sidigs.base.data.local.DevPreferenceManager
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

/**
 * @author Ahmad Amirudin.
 * @since 11-Mar-21.
 */

val preferenceModule = module {
    single { DevPreferenceManager(androidContext(), "AppPreference", get()) }
}