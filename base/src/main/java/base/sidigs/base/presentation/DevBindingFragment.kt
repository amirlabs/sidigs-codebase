package base.sidigs.base.presentation

import android.annotation.TargetApi
import android.content.Context
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.Toolbar
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import java.lang.reflect.ParameterizedType
import java.util.*

/**
 * Base class for Binding fragment
 * @sample base.sidigs.devcode.presentation.main.view.AllUserFragment
 * @constructor instance for fragment class
 * @property layoutResource the layout id of the fragment
 * @property currentActivity the activity where the fragment inflated
 * @author Ahmad Amirudin.
 * @since 11-Mar-21.
 */

abstract class DevBindingFragment<T:ViewDataBinding> : DevFragment() {

    override val layoutResource=0
    protected lateinit var binding: T

    private var persistentClass: Class<T>? = null
    init {
        this.persistentClass =
            (javaClass.genericSuperclass as ParameterizedType).actualTypeArguments[0] as Class<T>
    }

    /**
     * Method to inflate fragment's view
     * @param inflater Layout Inflater
     * @param container ViewGroup
     * @param savedInstanceState savedInstance
     * @return View that will be used in fragment
     */
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater,getLayoutResId(),container,false)
        return binding.root
    }


    protected fun getLayoutResId():Int{
        val resIdName = persistentClass?.simpleName?.dropLast(7)
        var layoutName = ""
        var lastIndex = 0
        if (resIdName != null) {
            for (i in 1..resIdName.lastIndex) {
                if (resIdName[i].isUpperCase()) {
                    layoutName += resIdName.substring(lastIndex, i) + "_"
                    lastIndex = i
                } else if (i == resIdName.lastIndex) {
                    layoutName += resIdName.substring(lastIndex, resIdName.length)
                }
            }
        }
        return resources.getIdentifier(
            layoutName.toLowerCase(Locale.getDefault()), "layout", context?.packageName
        )
    }
}