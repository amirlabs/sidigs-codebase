package base.sidigs.base.presentation.adapter

import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import base.sidigs.base.presentation.DevFragment

/**
 * Base for Pager adapter, can be used for ViewPager with TabLayout
 *
 * @Ahmad Amirudin.
 * @since 11-Mar-21.
 * @constructor create new instance of viewpager's adapter
 * @param fm Fragment Manager to switch between fragment, can be accessed from Activity or another fragment
 * @param listFragment List of Fragment that will be displayed, note : always use inherited class from DevFragment
 * @param listTitle List of String for tab title, nullable
 */

class DevViewPagerAdapter(
    fm: FragmentManager,
    private val listFragment: ArrayList<DevFragment>,
    private val listTitle: ArrayList<String>? = null
) : FragmentStatePagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    /**
     * Method to display fragment from selected position
     * @param position position of fragment in list
     * @return fragment to display
     */
    override fun getItem(position: Int) = listFragment[position]

    /**
     * Method get how many fragment attached to this adapter
     * @return amount of fragment attached to adapter
     */
    override fun getCount() = listFragment.size

    /**
     * Method to get page title
     * @param position position of title in list
     * @return page title if exist, if not call super method
     */
    override fun getPageTitle(position: Int) =
        if (listTitle?.size == listFragment.size) listTitle[position] else super.getPageTitle(position)
}