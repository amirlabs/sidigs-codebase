package base.sidigs.devcode.domain.usecase

import base.sidigs.base.data.ResultState
import base.sidigs.devcode.data.model.UserEntity
import base.sidigs.devcode.data.repositories.UserRepository
import base.sidigs.devcode.domain.model.User

class UserInteractor(private val repo: UserRepository) : UserUseCase {
    override suspend fun getUserFromPage(page: String): ResultState<ArrayList<User>> {
        return repo.getUsersPerPage(page).map { response ->
            return@map ArrayList<User>().apply {
                response.forEach { currentUser -> this.add(currentUser.toUserModel()) }
            }
        }
    }

    override suspend fun getUserFromDb(): ResultState<ArrayList<User>> {
        return repo.getUsersFromDb().map { response ->
            return@map ArrayList<User>().apply {
                response.forEach { currentUser -> this.add(currentUser.toUser()) }
            }
        }
    }

    override suspend fun saveUserToFavorite(userModel: UserEntity) =
        repo.saveUserToFavorite(userModel)

    override suspend fun removeUserFromFavorite(userModel: UserEntity) =
        repo.removeUserFromFavorite(userModel)
}