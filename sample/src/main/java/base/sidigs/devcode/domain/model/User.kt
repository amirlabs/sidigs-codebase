package base.sidigs.devcode.domain.model

import base.sidigs.devcode.data.model.UserEntity

data class User(
    val id: Int,
    val name: String,
    val avatar: String,
    val email: String
) {
    fun toUserEntity() = UserEntity(id = id, name = name, avatar = avatar, email = email)
}
