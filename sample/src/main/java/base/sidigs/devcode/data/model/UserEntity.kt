package base.sidigs.devcode.data.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.RoomDatabase
import base.sidigs.base.data.local.db.DbModel
import base.sidigs.devcode.domain.model.User

@Entity(tableName = "userTable")
data class UserEntity(
    @PrimaryKey
    @ColumnInfo(name = "id")
    val id: Int,

    @ColumnInfo(name = "name")
    val name: String,

    @ColumnInfo(name = "avatar")
    val avatar: String,

    @ColumnInfo(name = "email")
    val email: String
) : DbModel(){

    fun toUser() = User(id = id, name = name, avatar = avatar, email = email)
}
