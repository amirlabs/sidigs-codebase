package base.sidigs.devcode.data.local

import androidx.room.Dao
import androidx.room.Query
import base.sidigs.base.data.local.db.DevDao
import base.sidigs.devcode.data.model.UserEntity

@Dao
abstract class UserDao : DevDao<UserEntity> {

    @Query("SELECT * FROM userTable")
    abstract fun getAllFavoriteUser(): List<UserEntity>
}