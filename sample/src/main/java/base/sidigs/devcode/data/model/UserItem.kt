package base.sidigs.devcode.data.model

import base.sidigs.devcode.domain.model.User
import com.google.gson.annotations.SerializedName


data class UserItem(
    @SerializedName("last_name")
    val lastName: String? = null,

    @SerializedName("id")
    val id: Int? = null,

    @SerializedName("avatar")
    val avatar: String? = null,

    @SerializedName("first_name")
    val firstName: String? = null,

    @SerializedName("email")
    val email: String? = null
) {

    fun toUserModel() = User(
        id = id ?: Math.random().toInt(),
        name = firstName.orEmpty() + " " + lastName.orEmpty(),
        avatar = avatar.orEmpty(),
        email = email.orEmpty()
    )
}
