package base.sidigs.devcode

import android.app.NotificationChannel
import android.app.NotificationManager
import android.os.Build
import base.sidigs.base.DevApplication
import base.sidigs.devcode.di.dbModule
import base.sidigs.devcode.di.networkingModule
import base.sidigs.devcode.di.userModule
import base.sidigs.devcode.util.Variables
import org.koin.core.module.Module

class SampleApplication : DevApplication() {
    override fun initApplication() {}

    override fun defineKoinModules() = arrayListOf(dbModule, networkingModule, userModule)

    override fun defineNotificationChannel(): List<NotificationChannel> {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            listOf(
                NotificationChannel(
                    Variables.SAMPLE_CHANNEL_ID,
                    "Sample Notification",
                    NotificationManager.IMPORTANCE_HIGH
                )
            )
        } else {
            listOf()
        }
    }
}