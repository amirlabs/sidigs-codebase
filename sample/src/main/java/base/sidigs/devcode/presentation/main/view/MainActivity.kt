package base.sidigs.devcode.presentation.main.view

import android.content.Intent
import base.sidigs.base.presentation.DevBindingActivity
import base.sidigs.base.presentation.adapter.DevViewPagerAdapter
import base.sidigs.base.utils.core.SecurityCheck
import base.sidigs.base.utils.core.logDebug
import base.sidigs.base.utils.core.securityCheck
import base.sidigs.base.utils.extensions.toastInfo
import base.sidigs.devcode.R
import base.sidigs.devcode.databinding.ActivityMainBinding
import base.sidigs.devcode.presentation.form.view.FormActivity
import base.sidigs.devcode.presentation.main.viewmodel.MainViewModel
import org.koin.android.viewmodel.ext.android.viewModel

class MainActivity : DevBindingActivity<ActivityMainBinding>() {
    private val vm by viewModel<MainViewModel>()

    override fun initData() {
        when (securityCheck()) {
            SecurityCheck.EMULATOR, SecurityCheck.ROOTED -> {
                toastInfo(getString(R.string.labelDeviceNotSafe))
//                finishAffinity()
            }
            SecurityCheck.SAFE -> {
                logDebug ("Device is safe")
            }
        }
    }

    override fun initUI() {
        setupToolbar(binding.tbMain, getString(R.string.labelToolbarTitle), false, R.menu.menu_main) {
            when (it) {
                R.id.menuAdd -> {
                    startActivity(Intent(this, FormActivity::class.java))
                }
                R.id.menuError -> throw Exception("Does this help?")
            }
            false
        }
        binding.vpMain.apply {
            adapter = DevViewPagerAdapter(
                supportFragmentManager,
                arrayListOf(AllUserFragment(), FavoriteUserFragment()),
                arrayListOf("All", "Favorite")
            )
            binding.tlMain.setupWithViewPager(this)
        }
    }

    override fun initAction() {
        vm.getUsersFromPage(1.toString())
    }

    override fun initObserver() {}
}