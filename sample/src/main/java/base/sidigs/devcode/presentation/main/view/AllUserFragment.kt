package base.sidigs.devcode.presentation.main.view

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import base.sidigs.base.data.ResultState
import base.sidigs.base.presentation.DevBindingFragment
import base.sidigs.base.presentation.adapter.endless.OnLoadMoreListener
import base.sidigs.base.utils.extensions.displayDialog
import base.sidigs.base.utils.extensions.toastSuccess
import base.sidigs.base.utils.widget.MultiStateView
import base.sidigs.devcode.R
import base.sidigs.devcode.databinding.FragmentAllUserBinding
import base.sidigs.devcode.domain.model.User
import base.sidigs.devcode.presentation.main.adapter.UserAdapter
import base.sidigs.devcode.presentation.main.viewmodel.MainViewModel
import com.facebook.shimmer.ShimmerFrameLayout
import org.koin.android.viewmodel.ext.android.sharedViewModel

class AllUserFragment : DevBindingFragment<FragmentAllUserBinding>(), OnLoadMoreListener {

    private val vm by sharedViewModel<MainViewModel>()
    private var currentPage = 1
    private lateinit var userAdapter: UserAdapter

    override fun initData() {
    }

    override fun initUI() {
        userAdapter = UserAdapter({ user ->
            displayDialog(requireContext(), "${user?.name}\'s data", "Email : ${user?.email}", Pair("OK", {}))
        }, { user ->
            displayDialog(
                requireContext(),
                "Add to favorite?",
                "Are you sure adding ${user?.name} to favorite?",
                Pair("Yes", {
                    vm.saveUser(user) {
                        context.toastSuccess("${user?.name} added to favorite")
                    }
                }),
                Pair("No", {})
            )
        })

        binding.rvUserAll.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = userAdapter
        }

        userAdapter.apply {
            setLoadMoreListener(this@AllUserFragment)
            setEndlessScroll(binding.rvUserAll)
            resetEndlessScroll()
        }
    }

    override fun initAction() {
    }

    override fun initObserver() {
        vm.resultApi.observe(this, {
            when (it) {
                is ResultState.Loading -> {
                    binding.msvAll.showLoadingLayout()
                    binding.msvAll.getView(MultiStateView.ViewState.LOADING)?.findViewById<ShimmerFrameLayout>(R.id.sflUser)?.startShimmer()
                }

                is ResultState.Success -> {
                    binding.msvAll.getView(MultiStateView.ViewState.LOADING)?.findViewById<ShimmerFrameLayout>(R.id.sflUser)?.stopShimmer()
                    binding.msvAll.showDefaultLayout()
                    addUserToList(it.data)
                }

                is ResultState.Failure -> {
                    binding.msvAll.getView(MultiStateView.ViewState.LOADING)?.findViewById<ShimmerFrameLayout>(R.id.sflUser)?.stopShimmer()
                    binding.msvAll.showErrorLayout(R.mipmap.ic_launcher, "error", it.message, Pair("try again") {
                        vm.getUsersFromPage(currentPage.toString())
                    })
                }

                is ResultState.Empty -> {
                    binding.msvAll.getView(MultiStateView.ViewState.LOADING)?.findViewById<ShimmerFrameLayout>(R.id.sflUser)?.stopShimmer()
                    binding.msvAll.showEmptyLayout(R.mipmap.ic_launcher, "There's no user to display")
                }
                else -> {}
            }
        })
    }

    private fun addUserToList(listUser: ArrayList<User>) {
        userAdapter.addOrUpdateAll(listUser)
    }

    private fun loadMoreUser(page: String) {
        vm.loadMoreUser(page).observe(this, {
            when (it) {
                is ResultState.Loading -> userAdapter.showLoadMoreLoading()

                is ResultState.Success -> {
                    userAdapter.hideLoadMoreLoading()
                    addUserToList(it.data)
                }

                is ResultState.Failure -> userAdapter.showLoadMoreError()

                is ResultState.Empty -> userAdapter.finishLoadMore()
                else -> {}
            }
        })
    }

    override fun onLoadMore(skip: Int?, limit: Int?, totalItemsCount: Int?, view: RecyclerView?) {
        loadMoreUser(page = (++currentPage).toString())
    }

    override fun onLoadMoreRetryButtonClicked(skip: Int?, limit: Int?) {
        userAdapter.hideLoadMoreLoading()
        loadMoreUser(page = (++currentPage).toString())
    }
}